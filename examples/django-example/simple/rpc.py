from jsonrpc2.django.decorators import jsonrpc_method


@jsonrpc_method("django.get_current_user")
def get_current_user(request):
    """
    Bound function (e.g. **django** view)

    :returns: username or None
    """

    return request.user.username
