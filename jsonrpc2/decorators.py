import inspect

from jsonrpc2.dispatcher import dispatcher


def jsonrpc_method(name, **options):

    def wrapper(method):
        method.argspec = inspect.getargspec(method)

        if name and isinstance(name, basestring):
            dispatcher.register_method(name, method, **options)
        else:
            raise ValueError('Incorrect name for rpcmethod {}'
                             .format(method.__name__))
        return method
    return wrapper
