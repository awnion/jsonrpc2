import json
import uuid
import requests
from jsonrpc2.exceptions import InternalError, BaseRpcException


# noinspection PyProtectedMember
class RpcCall(object):
    slots = ('_client', '_name')

    def __init__(self, client, name):
        self._client = client
        self._name = name

    def __repr__(self):
        return self._name
    __str__ = __repr__

    @property
    def url(self):
        return self._client.url

    @property
    def name(self):
        return self._name

    def __getattr__(self, name):
        name = "%s%s%s" % (self._name, self._client.separator, name)
        return self.__class__(self._client, name)

    def __call__(self, *args, **kwargs):
        return self._client._call(self._name, *args, **kwargs)


class RpcClient(object):
    separator = '.'
    default_version = '2.0'
    default_timeout = 30

    def __init__(self, url, version=None, timeout=None, data=None):
        self.url = url
        self.version = version or self.default_version
        self.timeout = timeout or self.default_timeout
        self._data = {}
        if data:
            self._data.update(data)

    def _get_data(self, func_name, *args, **kwargs):
        id_ = uuid.uuid4().hex
        params = self.get_params(*args, **kwargs)
        return dict(
            id=id_,
            method=func_name,
            params=params,
            jsonrpc=self.version,
        )

    def _call(self, name, *args, **kwargs):
        data = self._get_data(name, *args, **kwargs)
        body = json.dumps(data).encode('utf-8')
        r = requests.post(self.url, body, timeout=self.timeout)
        if r.ok:
            try:
                # TODO: check id
                data = json.loads(r.content)
                try:
                    return data['result']
                except KeyError:
                    pass

                try:
                    raise BaseRpcException(data['error'].get('message'),
                                           data['error'].get('code'),
                                           data['error'].get('data'),
                                           data.get('_id'))
                except KeyError:
                    pass

                raise InternalError("Undefined rpc error")
            except ValueError:
                raise InternalError("Can't decode json")
        else:
            raise InternalError("Rpc request failed")

    def get_params(self, *args, **kwargs):
        """
        Create an array or positional or named parameters
        Mixing positional and named parameters in one
        call is not possible.
        """
        kwargs.update(self._data)
        if args and kwargs:
            raise ValueError('Cannot mix positional and named parameters')
        if args:
            return list(args)
        else:
            return kwargs

    def __getattr__(self, name):
        return RpcCall(self, name)
