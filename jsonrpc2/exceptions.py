class BaseRpcException(Exception):
    message = None
    code = None
    data = None
    id = None

    def __init__(self, message=None, code=None, data=None, id_=None):
        self.code = code or self.code
        self.message = message or self.message
        self.id = id_ or self.id
        self.data = data or self.data


class ParseError(BaseRpcException):
    code = -32700
    message = "Invalid JSON was received by the server"


class InvalidRequest(BaseRpcException):
    code = -32600
    message = "The JSON sent is not a valid Request object"


class MethodNotFound(BaseRpcException):
    code = -32601
    message = "The method does not exist or is not available"


class InvalidParams(BaseRpcException):
    code = -32602
    message = "Invalid method parameter(s)"


class InternalError(BaseRpcException):
    code = -32603
    message = "Internal JSON-RPC error"


class AuthenticationFailed(BaseRpcException):
    code = 1001
    message = "Authentication failed"


class PermissionDenied(BaseRpcException):
    code = 1002
    message = "Permission denied"


class BaseHttpException(Exception):
    pass


class Http400(BaseHttpException):
    pass


class Http401(BaseHttpException):
    pass


class Http402(BaseHttpException):
    pass


class Http403(BaseHttpException):
    pass


class Http404(BaseHttpException):
    pass


class Http405(BaseHttpException):
    pass
