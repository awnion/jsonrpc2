from django.core.management.base import BaseCommand

from jsonrpc2.django.models import RpcPermission
from jsonrpc2.dispatcher import dispatcher


class Command(BaseCommand):
    def handle(self, *args, **options):
        perms = set(dispatcher.get_methods_permissions())

        qs = RpcPermission.objects.filter(codename__in=perms)
        existing_perms = set(qs.values_list('codename', flat=True))

        missing_permission = perms - existing_perms

        for perm in missing_permission:
            RpcPermission.objects.create(name=perm, codename=perm)

            print "Adding permission '{}'".format(perm)
