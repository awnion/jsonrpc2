from django.conf.urls import patterns, url
from django.conf import settings

from jsonrpc2.django.views import RpcView
from jsonrpc2.django.utils.module_loading import simple_autodiscover_modules


if getattr(settings, 'JSONRPC_AUTODISCOVER', True):
    simple_autodiscover_modules('rpc')


urlpatterns = patterns(
    '',

    url('^$', RpcView.as_view(), name="rpc_view"),
)
