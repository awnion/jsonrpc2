from django.conf import settings
from django.contrib.auth import authenticate, login, logout

from jsonrpc2.django.decorators import jsonrpc_method
from jsonrpc2.exceptions import *


try:
    from django.contrib.auth import get_user_model
    User = get_user_model()
except ImportError:
    from django.contrib.auth.models import User


@jsonrpc_method('auth.login', login_required=False)
def auth_login(request, username, password, remember_me=False):
    """
    Authorizes a user to enable sending protected RPC requests
    """

    if not remember_me:
        request.session.set_expiry(0)
    else:
        request.session.set_expiry(settings.SESSION_COOKIE_AGE)

    try:
        user = User.objects.get(email=username)
        user = authenticate(username=user.username, password=password)
    except User.DoesNotExist:
        user = authenticate(username=username, password=password)

    if user is not None and request is not None:
        if user.is_active:
            login(request, user)
            return True
        else:
            raise NotFound(message='Account not found or disabled')

    raise AuthenticationFailed()


@jsonrpc_method('auth.logout', login_required=False)
def auth_logout(request):
    """
    Deauthorizes a user
    """

    if request is not None:
        logout(request)
        return True

    raise InternalError()
